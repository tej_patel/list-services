import { apiURL } from "./constants";
import ax from "axios";

export const axiosObject= ax.create({
  baseURL: apiURL,
  timeout: 50000,
});

axiosObject.interceptors.response.use(
  res => {console.log(res)},
  err => {
    throw err;
  }
);