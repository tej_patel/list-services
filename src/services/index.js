import { axiosObject } from '../config/axios'


export const getData = async () => {
    return await axiosObject.get();
  };