import './App.css';
import Page from './components'
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Page />
      </header>
    </div>
  );
}

export default App;
