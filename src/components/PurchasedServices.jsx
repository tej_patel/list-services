import { useEffect, useState, } from "react";

const PurchasedServices = ({data}) => {
    const [servicesWithCost, setServicesWithCost] = useState([])

    useEffect(() => {
        const services = [];
            data.map((mainService, index) => {
            return [...mainService.purchased_office_template.purchased_office_services.map((subService, index) => {
                return services.push({ serviceName: subService.name, cost: subService.price });
          })]
        })  
        setServicesWithCost(services);
    },[data])
  
  return (
    <>
          <h1 className='row col text-left mt-5'>Purchased Services</h1>
          <p className='text-left'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
          {data.map((mainService, index) => {
              return (
                mainService.purchased_office_template.purchased_office_services.length > 0 &&
                  <div className='main-service-wrapper my-3' key={index}>
                    <h2 className='main-service-name text-left'>
                        {mainService.name}
                      </h2>
                      {
                          mainService.purchased_office_template.purchased_office_services.map((subService, index) => {
                              return (
                                <div className='row my-3' key={index}>
                                <div className='col-3'>
                                    <img alt='service type' className='rounded' src={subService.image}></img>
                                      </div>
                                      <div className='col-6 text-left'>
                                          <h3>{subService.name}</h3>
                                          <br />
                                          <span>{subService.description}</span>
                                      </div>
                                      <div className='col-2 text-right my-auto mr-3'>
                                        Kr{` ${subService.price}`}
                                </div>
                            </div>
                              )
                          })
                      }
                     
                  </div>
                 
              )
          })}
          <TotalPriceComponent services={servicesWithCost} />
    </>
  );
};

export default PurchasedServices;


const TotalPriceComponent = ({ services }) => {
    
    return (
        <>
            <div className='total-cost-wrapper my-4 w-100'>
            {services.map((row, index) => {
                return (
                    <div className='row my-2 mx-3' key={index}>
                    <div className='col text-left'>
                        <h4>{row.serviceName}</h4>
                    </div>
                    <div className='col text-right'>
                        Kr {row.cost}
                    </div>
                </div>)
            })}
                <hr className='mx-3' style={   {
                    
                    'border': 0,
                    'borderTop': '1px solid white',
                    } }
                />
            <div className='row mx-3'>
                <div className='col-8 text-left'>
                    Total Costing:
                </div>
                <div className='col-4 text-right'>
                Kr {services.reduce(getSum,0)}
                </div>
            </div>
            </div>
           
        </>
    )
    
}

function getSum(total, currentRow) {
    return total + parseFloat(currentRow.cost);
}