
import { useEffect, useState } from "react";
import {getData} from '../services/index'
import PurchasedServices from './PurchasedServices'
import AdditionalServices from './AdditionalServices';
import {data} from '../config/constants'
const BothList = () => {
    
    const [purchasedServicesList, setPurchasedServicesList] = useState([]);
    const [additionalServicesList, setAdditionalServicesList] = useState([]);
    
  
    useEffect(() => {
        fetchData();
    }, []);
  
    const fetchData = async () => {
      try {
        //Note: Code to fetch data using the api endpoint is here and 
        //commented out because getting CORS errors from api,
        //in response of api one of the Header Access-Control-Allow-Origin is missing,
        //which can only be solved from server-side,
        //so have copied the data from api and stored it in a local variable to complete the task,
        //i hope you understand

        // const response = await getData();
        // console.log("🚀 ~ file: index.jsx ~ line 18 ~ fetchData ~ response", response)
        if (data.data?.purchased_services?.length > 0) {
          const temporaryPurcList = data.data.purchased_services.map((mainService, index) => {
            const selectedServices = mainService.purchased_office_template.purchased_office_services.filter((subService) => {
              return subService.service_selected;
            })
            return {
              ...mainService,
              purchased_office_template: {
                ...mainService.purchased_office_template,
                purchased_office_services: [...selectedServices]
              }
            }
          })
          const temporaryAddList = data.data.purchased_services.map((mainService, index) => {
            const notSelectedServices = mainService.purchased_office_template.purchased_office_services.filter((subService) => {
              return subService.service_selected === null || subService.service_selected === undefined;
            })
            return {
              ...mainService,
              purchased_office_template: {
                ...mainService.purchased_office_template,
                purchased_office_services: [...notSelectedServices]
              }
            }
          })
          setAdditionalServicesList(temporaryAddList);
          setPurchasedServicesList(temporaryPurcList);
        }
        
      }
      catch (error) {
      console.log("🚀 ~ file: index.jsx ~ line 20 ~ fetchData ~ error", error)
      }
    };
  
    
    return (
      <>
        <PurchasedServices data={purchasedServicesList}/>
        <AdditionalServices data={additionalServicesList} />
      </>
    );
  };
  
  export default BothList;
  