const AdditionalServices = ({data}) => {

  return (
    <>
      <h1 className='row col text-left mt-5'>Additional Services</h1>
          <p className='text-left'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
          {data.map((mainService, index) => {
              return (
                mainService.purchased_office_template.purchased_office_services.length > 0 &&
                  <div className='main-service-wrapper my-3' key={index}>
                    <h2 className='main-service-name text-left'>
                        {mainService.name}:
                      </h2>
                      {
                        
                          mainService.purchased_office_template.purchased_office_services.map((subService, index) => {
                              return (
                                <div className='row my-3' key={index}>
                                <div className='col-3'>
                                    <img alt='service type' className='rounded' src={subService.image}></img>
                                      </div>
                                      <div className='col-6 text-left'>
                                          <p>{subService.name}</p>
                                          <span>{subService.description}</span>
                                      </div>
                                      <div className='col-2 text-right my-auto mr-3'>
                                        Kr{` ${subService.price}`}
                                </div>
                            </div>
                              )
                          })
                      }
                     
                  </div>
                 
              )
          })}
    </>
  );
};

export default AdditionalServices;
